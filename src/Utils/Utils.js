const AWS = require('aws-sdk');

class Utils {
  static format(number) {
    return isNaN(number) ? 0 : Math.round(number * 10) / 10;
  }

  static percentage(partial, totalOrders) {
    if (totalOrders === 0) return 0
    return this.format((100 * partial) / totalOrders);
  }

  static getDiff(dateStart, dateEnd) {
    let count = 0;
    let curDate = dateStart;
    while (curDate <= dateEnd) {
      let dayOfWeek = curDate.getDay();
      let isWeekend = (dayOfWeek == 6) || (dayOfWeek == 0);
      if (isWeekend)
        count++;
      curDate = this.addDays(curDate, 1);
    }
    return Math.abs(dateEnd.getTime() - dateStart.getTime() - (this.daysToMilliseconds(count))); //Math.abs(dateStart.getTime() - dateEnd.getTime());
  }

  static getDiffDays(dateStart, dateEnd) {
    const diff = this.getDiff(dateStart, dateEnd);
    return Math.round(diff / (1000 * 3600 * 24));
  }

  static daysToMilliseconds(days) {
    return days * 24 * 60 * 60 * 1000;
  }

  static  addDays(a, days) {
    var date = new Date(a);
    date.setDate(date.getDate() + days);
    return date;
  }

  static  getDiffHours(dateStart, dateEnd) {
    const diff = this.getDiff(dateStart, dateEnd);
    return Math.round(diff / (1000 * 60 * 60));
  }

  static pascalToUnderscore(key) {
    return key.replace(/(?:^|\.?)([A-Z])/g, function (x, y) { return "_" + y.toLowerCase() }).replace(/^_/, "");
  }

  static todayDate = () => {
    const d = new Date();
    d.setUTCHours(0, 0, 0, 0);
    return d;
  }

  static todayDateISOString = () => {
    return this.todayDate().toISOString();
  }

  static catchError = (name) => {
    switch (name) {
      case "InvalidSignatureException":
        console.error('Invalid Signature Exception');
        break;
      case "ResourceNotFoundException":
        console.error('Resource Not Found Exception');
        break;
      case "FooServiceException":
        console.error('Foo Service Exception');
        break;
      default:
        throw new Error('Code error');
    }
  }

  getSecrets = async () => {
    let config = {}
    global.config.db_name = process.env.db_name
    if (process.env.NODE_ENV == 'local') {
      return new Promise(function (resolve, ) {
        global.config.db_read_host = process.env.db_read_host;
        global.config.db_write_host = process.env.db_write_host;
        global.config.db_username = process.env.db_username;
        global.config.db_password = process.env.db_password;
        global.config.db_port = process.env.db_port;

        resolve(global.config)
      })
    }
    else {
      const secretManager = new AWS.SecretsManager({ region: process.env.REGION });
      return new Promise(function (resolve, reject) {
        secretManager.getSecretValue({ SecretId: process.env.secrets }, (err, result) => {
          if (err) {
            reject(err)
            console.error(err);
          }
          else {
            const secret = JSON.parse(result.SecretString)
            global.config.db_read_host = secret.hostro;
            global.config.db_write_host = secret.host;
            global.config.db_username = secret.username;
            global.config.db_password = secret.password;
            global.config.db_port = secret.port;

            resolve(config)
          }
        });
      })

    }
  }

}
module.exports = Utils;