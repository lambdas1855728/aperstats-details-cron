const Db = require('../Utils/Db')

class StoreRepository {

  db;
  constructor() {
    this.db = Db.getInstance();
  }

  getOrders = async (idSupplier) => {
    const date_from = new Date();
    date_from.setMonth(date_from.getMonth() - process.env.months_orders_since);
    const date_from_str = date_from.toISOString().slice(0, 10).replace('T', '') + ' 23:59:59';
    const date_to = new Date();
    date_to.setDate(date_to.getDate() - 1);
    const date_to_str = date_to.toISOString().slice(0, 10).replace('T', '') + ' 23:59:59';
    const sql = `SELECT s.id_supplier,
                       s.name as seller,
                       o.id_order,
                       o.current_state,
                       o.date_add as order_date_add,
                       o.date_upd as order_date_upd
              FROM ps_supplier s
              LEFT JOIN ps_product p 
              ON p.id_supplier = s.id_supplier
              INNER JOIN ps_order_detail od 
              ON p.id_product = od.product_id
              LEFT JOIN ps_orders o on od.id_order = o.id_order
              WHERE o.date_add between "${date_from_str}" and "${date_to_str}" AND p.id_supplier <> 0 AND not ISNULL(p.id_supplier) and (${idSupplier} is null or s.id_supplier = ${idSupplier})
              GROUP BY s.id_supplier,s.name,o.id_order,o.total_products,o.current_state,o.date_add,o.date_upd
              ORDER BY s.id_supplier;`;
    console.debug(sql);
    return await this.db.query(sql);
  }

  getDeliveryTime = async (idOrders) => {
    const sql = `SELECT o.id_order,pago_aceptado.fecha_pagado,entregado.fecha_entregado
                FROM ps_orders o
                INNER JOIN (
                  SELECT oh2.id_order, oh2.date_add AS 'fecha_pagado'
                  FROM ps_order_history oh2
                  WHERE oh2.id_order_state = 2
                ) AS pago_aceptado
                ON pago_aceptado.id_order = o.id_order
                INNER JOIN (
                  SELECT oh3.id_order, oh3.date_add AS 'fecha_entregado'
                  FROM ps_order_history oh3
                  WHERE oh3.id_order_state = 5
                ) as entregado
                ON pago_aceptado.id_order = entregado.id_order
                WHERE o.id_order in (${idOrders})`;
    console.debug(sql);

    return await this.db.query(sql);
  }

  getMessages = async (idOrders) => {
    const sql = `SELECT ct.id_customer_thread,
                ct.id_order,
                ct.subclassification_id,
                ct.date_add as customer_thread_date_add,
                ct.date_upd as customer_thread_date_upd,
                cm.date_add as customer_message_date_add,
                UNIX_TIMESTAMP(ct.date_add) as customer_thread_date_add_timestamp,
                UNIX_TIMESTAMP(ct.date_upd) as customer_thread_date_upd_timestamp,
                UNIX_TIMESTAMP(cm.date_add) as customer_message_date_add_timestamp,
                ct.status as status_thread,
                cm.id_customer_message,
                cm.id_employee
                
          FROM ps_customer_thread ct 
          LEFT JOIN ps_customer_message cm 
          ON ct.id_customer_thread = cm.id_customer_thread
          WHERE ct.id_order in (${idOrders})
    `;
        
    const rta = await this.db.query(sql);

    console.debug(sql);
    return rta;
  }

  getDisabledSellers = async () => {
    const sql = 'select * from ps_cloudsearch_supplier_conf';
    return await this.db.query(sql);
  }

  enableSellers = async (idSuppliers) => {
    const sql = `delete from ps_cloudsearch_supplier_conf where id_supplier in (${idSuppliers.join(',')});`;
    console.debug(sql)
    return await this.db.execute(sql);
  }

  disableSellers = async (idSuppliers) => {
    const sql = idSuppliers.map(is=>`insert into ps_cloudsearch_supplier_conf set id_supplier = ${is}, disabled = 1`).join(';')
    console.debug(sql)
    return await this.db.execute(sql)
  }

}
module.exports = StoreRepository;