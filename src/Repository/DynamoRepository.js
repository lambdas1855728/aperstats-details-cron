const AWS = require('aws-sdk');
const Utils = require('../Utils/Utils');
const _ = require('lodash');

class DynamoRepository {
    db;
    detailsTable;
    globalTable;
    statTypeTable;
    featuresTable;

    constructor() {
        if (process.env.NODE_ENV === 'local') {
            this.db = new AWS.DynamoDB.DocumentClient({
                region: process.env.REGION,
                endpoint: process.env.DYNAMODB_ENDPOINT,
                credentials: {
                    accessKeyId: process.env.DYNAMODB_ACCESS_KEY_ID,
                    secretAccessKey: process.env.DYNAMODB_SECRET_ACCESS_KEY
                }
            });
        } else {
            this.db = new AWS.DynamoDB.DocumentClient({
                region: process.env.REGION,
            });
        }
        this.detailsTable = process.env.table_details;
        this.globalTable = process.env.table_global;
        this.statTypeTable = process.env.table_statType;
        this.featuresTable = process.env.table_features;
    }

    setStats = async (stats) => {
        console.log('Do setStats()');
        return Promise.all([
            this.setDetails(stats),
            this.setGlobalColor(stats)
        ])
        .then(r=>{
            console.log(r);
            return 'OK'
        })
    }

    setDetails = async (stats) => {
        console.log('Do setDetails()');
        const now = Utils.todayDate();
        stats.forEach(s => {
            s.created_at = now.toISOString();
            s.updated_at = now.toISOString();

        });
        return await this.saveBatch(stats, this.detailsTable);
    }

    setGlobalColor = async (stats) => {
        console.log('Do setGlobalColor()');
        console.log(stats);
        const globalColors = stats.map( s => ({ id_supplier: s.id_supplier, global_color: s.global_color, score: s.score, active_features: s.active_features, first_yellow_update: s.first_yellow_update }));
        return await this.saveBatch(globalColors, this.globalTable);
    }

    getSellerGlobal = async (idSupplier) => {
        console.log('Do getSellerGlobal()');
        const params = {
            TableName: this.globalTable,
            KeyConditionExpression: "id_supplier = :id_supplier",
            ExpressionAttributeValues: {
                ":id_supplier": idSupplier
            }
        };

        try {
            return this.db.query(params).promise();
        } catch ({ name }) {
            Utils.catchError(name);
        }
    }

    getGlobals = async () => {
        console.log('Do getGlobals()');
        const params = {
            TableName: this.globalTable
        };

        try {
            return await this.db.scan(params).promise();
        } catch ({ name }) {
            Utils.catchError(name);
        }
    }

    getSupplierStatsDetails = async (idSupplier) => {
        console.log('Do getSupplierStatsDetails()');
        const d = Utils.todayDate();
        d.setMonth(d.getMonth() - process.env.months_to_become_red);
        const params = {
            TableName: this.detailsTable,
            KeyConditionExpression: "id_supplier = :id_supplier and created_at >= :created_at",
            ExpressionAttributeValues: {
                ":id_supplier": idSupplier,
                ":created_at": d.toISOString()
            }
        };

        try {
            return await this.db.query(params).promise();
        } catch ({ name }) {
            Utils.catchError(name);
        }
    }

    getStatTypes = async () => {
        console.log('Do getStatTypes()');
        const params = {
            TableName: this.statTypeTable,
            ProjectionExpression: 'id, stat_name, stat_key, stat_threshold, stat_unit, stat_comparison'
        };

        try {
            return await this.db.scan(params).promise();
        } catch ({ name }) {
            Utils.catchError(name);
        }
    }

    getFeature = async (feature_name) => {
        console.log('Do getFeature()');
        const params = {
            TableName: this.featuresTable,
            IndexName: "feature_name-index",
            KeyConditionExpression: "feature_name = :feature_name",
            ExpressionAttributeValues: {
                ":feature_name": feature_name
            }
        }

        try {
            return await this.db.query(params).promise();
        } catch ({ name }) {
            Utils.catchError(name);
        }
    }

    clearItems = async () => {
        console.log('Do clearItems()');
        const params1 = {
            TableName: this.detailsTable
        };

        try {
            const items = await this.db.scan(params1).promise();
            await Promise.all(items.Items.map(async ({ id_supplier }) => {
                const params2 = {
                    TableName: this.detailsTable,
                    Key: { id_supplier }
                };
                await this.db.delete(params2).promise();
            }));
        } catch ({ name }) {
            Utils.catchError(name);
        }
    }

    saveBatch = (arr, table) => {
        // Map Entries: Maximum number of 25 items.
        const self = this;
        return new Promise(function (resolve, reject) {
            
            let itemsArray = [];
            for (let i = 0; i < arr.length; i++) {
                const item = {
                    PutRequest: {
                        Item: arr[i]
                    }
                };

                if (item) {
                    itemsArray.push(item);
                }
            }

            for(const chunk of _.chunk(itemsArray,20)){
                const params = {
                    RequestItems: {}
                };
                params['RequestItems'][table] = chunk

                self.db.batchWrite(params, function (err, ) {
                    if (err) {
                        console.log(JSON.stringify(chunk))
                        reject(err)
                    }
                    else {
                        console.log('Added ' + chunk.length + ' items to DynamoDB');
                        
                    }
                });
            }

            resolve(true);

        })

    }

}
module.exports = DynamoRepository;