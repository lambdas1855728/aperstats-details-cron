const StoreRepository = require('../Repository/StoreRepository');
const DynamoRepository = require('../Repository/DynamoRepository');
const CalculatorService = require('./CalculatorService');
const SearchCloudService = require('./SearchCloudService');

const Utils = require('../Utils/Utils')

const Order = require('../Entity/Order')
const _ = require('lodash');
const { Stat } = require('../Stats/Stat');

class DetailsService {

  calculator;
  storeRepository;
  dynamoRepository;
  searchCloud;

  constructor() {
    this.calculator = new CalculatorService();
    this.storeRepository = new StoreRepository();
    this.searchCloud = new SearchCloudService(this.storeRepository);
    this.dynamoRepository = new DynamoRepository();
  }

  procesar = async (idSupplier) => {
    await this.calculator.init();

    const orders = await this.storeRepository.getOrders(idSupplier);
    console.debug('Total Ordenes: ',orders.length)

    if (orders.length > 0){
      const stats = []
      const suppliers = _.uniqWith(orders,(val_a,val_b)=>val_a.id_supplier == val_b.id_supplier).map(x=>({seller: x.seller,idSupplier:x.id_supplier}));
      console.debug('Total Suppliers: ',suppliers.length)
      
      // Todo maybe not necessary
      for (const supplier of suppliers){
        const supplierOrders = _.filter(orders,o=>o.id_supplier==supplier.idSupplier).map(o=>new Order(o));
        console.debug('Supplier '+supplier.idSupplier+' ordenes: ',supplierOrders.length)
        
        //new stat object
        const stat = new Stat(supplier.idSupplier,supplier.seller)
        const calculatedFields = Stat.initCalculatedFields()

        for(const cf of calculatedFields){
          const statKeyName = Utils.pascalToUnderscore(cf.constructor.name);
          stat[statKeyName] = await this.calculator.buildCalculatedStat(cf,statKeyName,supplierOrders)
          stat.score += stat[statKeyName].threshold_distance;
        }

        const global = (await this.dynamoRepository.getSellerGlobal(supplier.idSupplier)).Items[0];
        stat.orders_amount = supplierOrders.length;
        stat.global_color = await this.calculator.globalColor(stat);
        stat.first_yellow_update = await this.calculator.firstYellowUpdate(global, stat.global_color);
        stat.score = stat.score.toFixed(2);
        stat.active_features = global !== undefined && global.active_features !== undefined ? global.active_features : true;
       
        stats.push(stat)
      }
      
      await this.dynamoRepository.setStats(stats);
      await this.manageSearchCloud(stats);
      return stats;
    }
    return 'NO ORDERS'
  }

  manageSearchCloud = async (stats) => {
    await this.searchCloud.init();
    return await this.searchCloud.handleSellers(stats);
  }
  
}
module.exports = DetailsService;