const DynamoRepository = require('../Repository/DynamoRepository');
const StoreRepository = require('../Repository/StoreRepository');

class SearchCloudService {
    
    sellersToDisable;
    sellersToEnable;
    disabledSellers;
    featureActive;
    dynamoRepository;
    storeRepository;

    constructor(){
        this.sellersToDisable   = [];
        this.sellersToEnable    = [];
        this.dynamoRepository   = new DynamoRepository();
        this.storeRepository    = new StoreRepository();
    }

    init = async () => {
        this.disabledSellers = await this.storeRepository.getDisabledSellers();
        this.featureActive = (await this.dynamoRepository.getFeature('aperSearchCloud')).Items[0].feature_active;
    }
    
    handleSellers = async (stats) => {

        stats.forEach(stat => {
            if(stat.global_color === 'red' && this.isActive(stat)) {
                this.sellersToDisable.push(stat.id_supplier);
            }else{
                this.sellersToEnable.push(stat.id_supplier);
            }
        });

        if (this.sellersToEnable.length > 0)
            await this.storeRepository.enableSellers(this.sellersToEnable);
        
        if (this.sellersToDisable.length > 0)
            await this.storeRepository.disableSellers(this.sellersToDisable);
    }
    
    isActive = (stat) => {
        return this.featureActive && stat.active_features;
    }
}
module.exports = SearchCloudService;