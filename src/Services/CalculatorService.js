const Utils = require('../Utils/Utils');
const DynamoRepository = require('../Repository/DynamoRepository');
const MONTHS_TO_BECOME_RED = process.env.months_to_become_red;

class Calculator {
  dynamoRepository;
  statEntities;
  utils;
  reset_date;

  constructor(){
    this.reset_date       = "1970-01-01T00:00:00.000Z";
    this.dynamoRepository = new DynamoRepository();
  }

  init = async () =>  {
    this.statEntities = (await this.dynamoRepository.getStatTypes()).Items;
  }

  getStatEntity = (key) => {
    return this.statEntities.find((stat) => stat.stat_key === key);
  }

  getDistance = (statEntity, value) => {
    return Utils.format(value - statEntity.stat_threshold);
  }

  color = (statEntity, value) => {
    if(statEntity.stat_comparison === '>'){
      return value > statEntity.stat_threshold ? 'red' : 'green';
    }else if(statEntity.stat_comparison === '<'){
      return value < statEntity.stat_threshold ? 'red' : 'green';
    }

    throw 'Error, no comparison defined.';
  }

  globalColor = async (stat) => {

    if (!this.haveEnoughSales(stat)) {
      return 'yellow';
    }
    let color = 'green';
    if(this.isYellow(stat)){
      const global = (await this.dynamoRepository.getSellerGlobal(stat.id_supplier)).Items[0];
      
      color = this.shouldBeRed(global) ? 'red' : 'yellow';
    }
    return color;
  }
  
  haveEnoughSales = (stats) => {
    const minimumSales = (this.getStatEntity('minimum_sales')).stat_threshold;
    return stats.orders_amount >= minimumSales;
  }
  
  isYellow = (stat) => {
    console.log(stat)
    return Object.keys(stat).filter((s) => typeof(stat[s]) === 'object' && stat[s].global_color === 'red').length > 0;
  }
  
  shouldBeRed = (global) => {
    if(global === undefined || global.first_yellow_update === undefined || !global.first_yellow_update)
      return false;
    const reset_date = new Date(this.reset_date); 
    reset_date.setHours(0,0,0,0);
    const first_yellow_update = new Date(global.first_yellow_update); 
    first_yellow_update.setHours(0,0,0,0); 
    const d = new Date(); d.setMonth(d.getMonth() - MONTHS_TO_BECOME_RED); 
    d.setHours(0,0,0,0);
    if((first_yellow_update - reset_date) === 0)
      return false;
    return first_yellow_update < d;
  }

  firstYellowUpdate = (global, newColor) => {
    if(global === undefined)
      return this.reset_date;
    if(global.first_yellow_update === null && newColor!== 'yellow')
      return this.reset_date;
    if(newColor === 'green')
      return this.reset_date;
    const oldColor = global.global_color;  
    if(newColor === 'yellow' && oldColor === 'green')
      return Utils.todayDateISOString();
    if(newColor === 'yellow' && oldColor === 'red')
      return Utils.todayDateISOString();   
    return global.first_yellow_update;
  }

  buildCalculatedStat =  async (stat,key,orders) => {
    const value = await stat.calculate(orders);
    const statEntity = this.getStatEntity(key);
    return {
      name: statEntity.stat_name,
      value: value,
      threshold_distance: this.getDistance(statEntity, value),
      color: this.color(statEntity, value),
      unit: statEntity.stat_unit,
    };
  }
}
module.exports = Calculator;