const Utils = require('../Utils/Utils');

class DeliveredOrders {

  async calculate(orders) {
    console.log('-------------------------DELIVEREDORDERS-------------------------')
    const unDeliveredOrders = orders.filter(o=>o.isUndelivered).length
    const deliveredOrders = orders.filter(o=>o.isDelivered).length
    const unDeliveredAndDeliveredOrders = unDeliveredOrders + deliveredOrders

    const percent = Utils.percentage(
      deliveredOrders,
      unDeliveredAndDeliveredOrders
    );

    console.log('deliveredOrders: ',deliveredOrders,' UndeliveredOrders: ',unDeliveredOrders);

    console.log('-------------------------/DELIVEREDORDERS-------------------------')

    return Promise.resolve(percent)
  }

}
module.exports = DeliveredOrders;