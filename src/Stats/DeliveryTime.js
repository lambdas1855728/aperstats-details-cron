
const StoreRepository = require('../Repository/StoreRepository')
const _ = require('lodash');
const Utils = require('../Utils/Utils');

class DeliveryTime {
  
  async calculate(orders) {
    console.log('-------------------DELIVERYTIME-------------------')
    //if any of the orders pass the delivered state
    const storeRepository = new StoreRepository();
    
    if (orders.length == 0)
      return 0

    const queryDelivered = await storeRepository.getDeliveryTime(_.map(orders,'idOrder').join(','))
    
    if (queryDelivered.length == 0)
      return 0
    
    const countDays = queryDelivered.reduce((acc,q) => {
      const diffDays = Utils.getDiffDays(q.fecha_pagado,q.fecha_entregado);
      if (diffDays < 0){
        console.error('Delivered date < Payment date',q)
        return acc
      }
      //If pay day same delivered day = 1
      return acc + ( (diffDays == 0) ? 1 : diffDays )
    }, 0);
    console.log('cant dias: ',countDays, 'Delivered Orders: ',queryDelivered.length);

    console.log('-------------------/DELIVERYTIME-------------------')
    let days = countDays / queryDelivered.length;
    return days
  }
}

module.exports = DeliveryTime;