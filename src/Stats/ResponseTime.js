
const StoreRepository = require('../Repository/StoreRepository');
const Message = require('../Entity/Message')
const Utils = require('../Utils/Utils');
const _ = require('lodash');

class ResponseTime {

  async calculate(orders) {
    
    console.log('-------------------------RESPONSETIME-------------------------')
    
    const storeRepository = new StoreRepository();
    //recuperar mensajes
    const messagesDb = await storeRepository.getMessages(_.map(orders,'idOrder').join(','))
    const messages = messagesDb.map(x=>new Message(x))

    //orderby and group by thread
    const groupedOrderedMessages = _(messages).orderBy(['idCustomerThread', 'messageDateAddTimestamp'],['asc', 'asc']).groupBy('idCustomerThread').value();

    console.log(groupedOrderedMessages)

    let accResponseTime = 0;
    for (const key of Object.keys(groupedOrderedMessages)){
      accResponseTime += this.processThread(groupedOrderedMessages[key])
    }

    console.log('Response Time:: ',accResponseTime)
    console.log('-------------------------/RESPONSETIME-------------------------')

    return Utils.format(accResponseTime);
  }

  processThread(thread){

    let uni = []
    const idCustomerThread = thread[0].idCustomerThread

    do{
      const idxTEmployee = thread.findIndex(t=>t.isEmployeeReply)
      if(idxTEmployee !== -1){
        uni.push(Utils.getDiffHours(thread[0].messageDateAdd,thread[idxTEmployee].messageDateAdd))

        thread.shift()

        const idxClient = thread.findIndex(t=>!t.isEmployeeReply)
        if(idxClient === -1)
          thread = []
        else
          thread.splice(0,idxClient)
      }
      else{
        thread = []
      }
    }
    while (thread.length > 0)

    console.log('Thread Id: ',idCustomerThread,' arr: ',uni)

    if (uni.length == 0)
      return 0

    return uni.reduce((acc, a) => acc + a, 0) / (uni.length * 2)
  }
}
module.exports = ResponseTime;