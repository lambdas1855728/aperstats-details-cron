const StoreRepository = require('../Repository/StoreRepository')
const Message = require('../Entity/Message')
const _ = require('lodash')
const Utils = require('../Utils/Utils');

class ClaimsByOrders {

  async calculate(orders) {
    
    console.log('-------------------------CLAIMSBYORDER-------------------------')
    const storeRepository = new StoreRepository();
    const approvedOrders = orders.filter(o=>o.isApproved);

    if (approvedOrders.length > 0){
      const messagesDb = await storeRepository.getMessages(_.map(approvedOrders,'idOrder').join(','))
      const messages = messagesDb.map(m=>new Message(m))
  
      const uniqThreadsMessages = _.uniqBy(messages,'idCustomerThread')
      //console.log(uniqThreadsMessages)
      
      const cantClaims = uniqThreadsMessages.filter(t=>t.isClaim).length
      
      console.log('idSupplier: '+ approvedOrders[0].idSupplier +' total claims: ' + cantClaims + ' Total Orders approved:' +approvedOrders.length);
      
      return Utils.percentage(cantClaims, approvedOrders.length);
    }
    console.log('-------------------------/CLAIMSBYORDER-------------------------')
    return 0
  }
}
module.exports = ClaimsByOrders;