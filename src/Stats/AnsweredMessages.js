const Utils = require('../Utils/Utils');
const StoreRepository = require('../Repository/StoreRepository')
const Message = require('../Entity/Message')
const _ = require('lodash')

class AnsweredMessages {
 
  async calculate(orders) {
    const storeRepository = new StoreRepository();
    // recupero mensajes
    const messagesDb = await storeRepository.getMessages(_.map(orders,'idOrder').join(','))
    const messages = messagesDb.map(m=>new Message(m))
    // agrupo y unifico por hilo abierto y ultimo mensaje
    const lastMessageOpenThreads = _(messages).orderBy(['idCustomerThread', 'messageDateAddTimestamp'],['asc', 'desc']).uniqBy('idCustomerThread').filter(['statusThread','open']).value();
    //.orderBy(_.uniqBy('idCustomerThread'))
    console.log(lastMessageOpenThreads);
    // if ultimo mensaje del cliente totalClaims++
    const totalClaims = (lastMessageOpenThreads.filter(m=>!m.isEmployeeReply).length == 0) ? 100 : lastMessageOpenThreads.filter(m=>!m.isEmployeeReply).length
    // if ultimo mensaje de employee totalAnswers++
    const totalAnswers = (lastMessageOpenThreads.filter(m=>m.isEmployeeReply).length == 0  && lastMessageOpenThreads.filter(m=>!m.isEmployeeReply).length == 0) ? 100 : lastMessageOpenThreads.filter(m=>m.isEmployeeReply).length 

    console.log('ANSWEREDMESSAGES:: totalClaims: ',totalClaims,' totalAnswers: ',totalAnswers)

    // devuelvo porcentaje (totalAnswers,totalClaims)
    return Utils.percentage(totalAnswers, totalClaims);
  }
}
module.exports = AnsweredMessages;