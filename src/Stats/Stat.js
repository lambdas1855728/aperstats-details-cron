const DeliveryTime = require('../Stats/DeliveryTime');
const DeliveredOrders = require('../Stats/DeliveredOrders');
const ClaimsByOrders = require('../Stats/ClaimsByOrders');
const AnsweredMessages = require('../Stats/AnsweredMessages');
const ResponseTime = require('../Stats/ResponseTime');


class Stat {
  id_supplier
  seller
  score
  active_features
  created_at
  updated_at

  constructor(id_supplier,seller){
    this.id_supplier = id_supplier;
    this.score = 0;
    this.seller = seller;
  }

  static initCalculatedFields = () => {
    return [
      new DeliveryTime,
      new DeliveredOrders,
      new ClaimsByOrders,
      new AnsweredMessages,
      new ResponseTime,
    ];
  };
}
exports.Stat = Stat;