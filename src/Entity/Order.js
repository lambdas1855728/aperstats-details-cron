const UNDELIVEREDSTATES = process.env.order_undelivered_state.split(',').map(Number);
const NOTAPPROVEDSTATES = process.env.order_unapproved_states.split(',').map(Number);
const DELIVEREDSTATE =  Number(process.env.order_delivered_state);

class Order{
    idSupplier
    seller
    idOrder
    currentState
    orderDateAdd
    orderDateUpd

    isDelivered = false
    isUndelivered = false
    isApproved = false

    constructor(dborder){
        this.idSupplier = Number(dborder.id_supplier);
        this.seller = dborder.seller;
        this.idOrder = Number(dborder.id_order);
        this.currentState = Number(dborder.current_state);
        this.orderDateAdd = dborder.order_date_add;
        this.orderDateUpd = dborder.order_date_upd

        this.isDelivered = this.currentState === DELIVEREDSTATE;
        this.isUndelivered = UNDELIVEREDSTATES.indexOf(this.currentState) !== -1;
        this.isApproved =  NOTAPPROVEDSTATES.indexOf(this.currentState) === -1;
    }
}

module.exports = Order