//const subCategoriesAnswers = process.env.claims_sub_categories_for_answers.split(',').map(Number);
const subCategories = process.env.claims_sub_categories.split(',').map(Number);

class Message {

    idCustomerThread
    subClassificationId
    threadDateAdd
    threadDateUpd
    idCustomerMessage
    idEmployee
    messageDateAdd
    idOrder
    statusThread

    isClaim = false
    isEmployeeReply = false
 
    constructor(dbmessage){
        this.idCustomerThread = dbmessage.id_customer_thread;
        this.subClassificationId = dbmessage.subclassification_id;
        this.threadDateAdd = dbmessage.customer_thread_date_add;
        this.threadDateAddTimestamp = dbmessage.customer_thread_date_add_timestamp;
        this.threadDateUpd = dbmessage.customer_thread_date_upd;
        this.threadDateUpdTimestamp = dbmessage.customer_thread_date_upd_timestamp;
        this.idCustomerMessage = dbmessage.id_customer_message;
        this.idEmployee = dbmessage.id_employee;
        this.messageDateAdd = dbmessage.customer_message_date_add;
        this.messageDateAddTimestamp = dbmessage.customer_message_date_add_timestamp;
        this.idOrder = dbmessage.id_order
        this.statusThread = dbmessage.status_thread

        this.isClaim = subCategories.indexOf(this.subClassificationId) !== -1;
        this.isEmployeeReply = this.idEmployee !== 0
    }
}


module.exports = Message