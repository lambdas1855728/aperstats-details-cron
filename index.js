const DetailsService = require('./src/Services/DetailsService');
const Util = require('./src/Utils/Utils')
global.config= {
  db_write_host: '',
  db_read_host: '',
  db_username: '',
  db_password: '',
  db_port: '',
  db_name: '',
}

exports.handler = async (event) => {
  const util = new Util()
  await util.getSecrets();

  const idSupplier = event.idSupplier || null;
 
  let service = new DetailsService();
  return await service.procesar(idSupplier);
};

